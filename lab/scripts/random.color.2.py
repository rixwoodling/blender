#!/usr/bin/env python
# -*- coding: utf-8 -*-

def get_random_color():
    r, g, b = [random.random() for i in range(3)]
    return r, g, b, 1
obj_color()    

obj = bpy.ops.mesh.primitive_cube_add()
obj.color = get_random_color()

