#!/usr/bin/env python
# -*- coding: utf-8 -*-

import bpy

mat =bpy.data.materials["Material"]

nodes = mat.node_tree.nodes
material_output = nodes.get("Material Output")

node_texture = nodes.new(type='ShaderNodeTexImage')
node_texture.image = bpy.data.images.load("./images/image.jpg")
node_texture.projection = "Box"
node_texture.location = -150,300

uv_node = nodes.new(type='ShaderNodeTexCoord')
uv_node.location = -350,300

links = mat.node_tree.links
link = links.new(node_texture.outputs[0], nodes.get("Diffuse BSDF).inputs[0])
link_uv = links.new(uv_node.outputs[2], node_texture.inputs[0])

