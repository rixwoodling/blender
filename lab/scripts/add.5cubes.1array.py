#!/usr/bin/env python
# -*- coding: utf-8 -*-

import bpy
import mathutils
import math

x = 0
y = -5
z = 0

addcube = bpy.ops.mesh.primitive_cube_add

for i in range(5):
    addcube(location=(x, y, z))
    x = x + 0
    y = y + 3
    z = z + 0
    for obj in bpy.context.selected_objects:
        obj.name = "cube.%d"%(i + 1)
        
        