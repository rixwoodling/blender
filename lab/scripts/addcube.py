import bpy
import mathutils
import math
import time

x = 0
y = 0
z = 0
a = 0
b = 0
c = 0

addcube = bpy.ops.mesh.primitive_cube_add
rotatecube = bpy.ops.transform.translate

for i in range(5):
    addcube(location=(x, y, z))
    time.sleep(1)
    rotatecube(value=(a, b, c))
    x = x + 1
    y = y + 1
    z = z + 1
    a = a + 1
    b = b + 1
    c = c + 1
        
#time.sleep(1)
#bpy.ops.object.delete(use_global=False)