#!/usr/bin/env python
# -*- coding: utf-8 -*-

import bpy

def createnamelocate_cube():
    # create a new cube
    bpy.ops.mesh.primitive_cube_add()
    # newly created cube will be automatically selected
    cube = bpy.context.selected_objects[0]
    # change name
    cube.name = "newcube"
    # change its location
    cube.location = (0.0, 0.0, 0.0)
createnamelocate_cube()

