#!/usr/bin/env python
# -*- coding: utf-8 -*-

import bpy

# replace material name
def replace_materialname():
  for material in bpy.data.materials:
    material.name = 'newname.material'
replace_materialname()    

# remove all unused materials
def remove_unusedmaterials():
  for material in bpy.data.materials:
    if not material.users:
      bpy.data.materials.remove(material)
remove_unusedmaterials()        

