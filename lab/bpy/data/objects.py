#!/usr/bin/env python
# -*- coding: utf-8 -*-

import bpy

# select cube via python
def selectcube2():
    bpy.data.objects['Cube'].select = True
selectcube2()

# rename object or objects (auto sequence)
def rename_objects():
    for obj in bpy.data.objects:
        obj.name = 'newobjectname'
rename_objects()
        
# select all objects in scene
def select_allobjects():
    for obj in bpy.data.objects:
        obj.select = True
select_allobjects() 

# print all objects (run in console)
def print_allobjects():
    for obj in bpy.data.objects:
        print(obj.name)
print_allobjects() 

