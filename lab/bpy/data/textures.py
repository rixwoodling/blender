#!/usr/bin/env python
# -*- coding: utf-8 -*-

import bpy

# replace texture name
def replace_texturename():
  for texture in bpy.data.textures:
    texture.name = 'newname.texture'
replace_texturename()    

