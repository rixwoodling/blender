#!/usr/bin/env python
# -*- coding: utf-8 -*-

import bpy 

# replace mesh name with object name
def objectname2mesh():
  bpy.context.active_object.data.name = bpy.context.active_object.name
objectname2mesh()

# replace material name with object name
def objectname2material():
  bpy.context.active_object.active_material.name = bpy.context.active_object.name
objectname2material()  

# print active material name
def print_activematerial():
    print(bpy.context.active_object.active_material.name)
print_activematerial()