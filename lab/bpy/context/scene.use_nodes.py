#!/usr/bin/env python
# -*- coding: utf-8 -*-

import bpy

# check use shader nodes in node editor
def usenodes():
  bpy.context.scene.use_nodes = True
usenodes()

