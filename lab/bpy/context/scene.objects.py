#!/usr/bin/env python
# -*- coding: utf-8 -*-

import bpy 

# deselect all or select none
def deselectall():
  for obj in bpy.context.scene.objects: 
    obj.select = False
deselectall()

# deselect all only if visible
def selectifvisible():
  for obj in bpy.context.scene.objects:
    if obj.hide == False:  
      obj.select = False
selectifvisible()  
  
# rename objects if names start with 'c'
def renameifstartswith():
    for obj in bpy.context.scene.objects:
        if obj.type == 'MESH' and obj.name.lower().startswith("c")
            obj.name = "newName"
renameifstartswith()

# select 'Cube' via python
def selectcube1():
    bpy.context.scene.objects.active = bpy.data.objects['Cube']
selectcube1()

