#!/usr/bin/env python
# -*- coding: utf-8 -*-

import bpy

# sets object to edit mode if in another mode
def modesetedit():
  bpy.ops.object.mode_set(mode='EDIT_MODE')
modesetedit()

# sets object to object mode if in another mode
def modesetobject():
  bpy.ops.object.mode_set(mode='OBJECT')
modesetobject()  

