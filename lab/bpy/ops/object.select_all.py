#!/usr/bin/env python
# -*- coding: utf-8 -*-

import bpy

# deselect everything in the scene
def deselectall():
  bpy.ops.object.select_all(action='DESELECT')
deselectall()

