#!/usr/bin/env python
# -*- coding: utf-8 -*-

import bpy

# adds default cube at 3d cursor
def addcube():
    bpy.ops.mesh.primitive_cube_add()
addcube()

# adds default cube at specific location
def addcube_xyz():
  x = 0
  y = 0
  z = 0
  bpy.ops.mesh.primitive_cube_add(location=(x, y, z))
addcube_xyz()

# adds an array of 5 cubes in the y direction
def add5cubearray():
    y = 0
    for i in range(5):
        bpy.ops.mesh.primitive_cube_add(location=(0, y, 0))
        y = y + 3
add5cubearray()    

